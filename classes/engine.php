<?php
/* Подключаем модули, классы и т.п. */
require_once __DIR__.'/../config.php';
require_once $modules['database'];

class LinkShortenerEngine
{
  private
    $db, $url, $redirect_url, $referer, $user_agent;

  function __construct()
  {
    global $database_data;
    $this->db = new MysqliDb($database_data);

    $this->url        = $_SERVER['REQUEST_URI'];
    $this->ip         = $_SERVER['REMOTE_ADDR'];
    $this->referer    = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null);
    $this->user_agent = (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null);
  }

  public function findRedirect()
  {
    if (isset($this->url))
    {
      $record = self::findShortUrl($this->url);
      if ($record !== false)
      {
        self::increaseLinkCount(
          $record['id'],
          $this->ip,
          $this->referer,
          $this->user_agent
        );

        return $record['url'];
      }

    }

    return false;
  }

  private function increaseLinkCount($id, $ip, $referer, $user_agent)
  {
    $data = [
      'count' => $this->db->inc(1)
    ];

    $this->db->where('link_id', $id);
    $this->db->where('ip', $ip);

    if (!$this->db->has('statistics'))
    {
      $data['unic_count'] = $this->db->inc(1);
    }

    $this->db->insert('statistics', [
      'link_id'    => $id,
      'ip'         => $ip,
      'referer'    => $referer,
      'user_agent' => $user_agent
    ]);

    $this->db->where('id', $id);
    $this->db->update('links', $data);
  }

  private function findShortUrl($url, $access = 'user')
  {
    $this->db->where('shorturl', $url);
    if ($access == 'user')
    {
      $this->db->where('disabled', 0);
    }
    $record = $this->db->getOne('links');

    if (!empty($record))
    {
      return $record;
    }

    return false;
  }

  public function addShortUrl($url, $permission, $ip, $user_agent, $shorturl = false, $disabled = 0, $password = null)
  {
    $data = [
      'url' => strpos($url, ':') ? $url : 'http://'.$url,
      'shorturl' => $shorturl === false ? null : $shorturl,
      'permission' => $permission,
      'count' => 0,
      'unic_count' => 0,
      'disabled' => $disabled === 0 ? 0 : 1,
      'password' => $password,
      'ip' => $ip,
      'user_agent' => $user_agent
    ];

    $id = $this->db->insert('links', $data);
    if ($id)
    {
      if ($shorturl === false)
      {
        $shorturl = '/R'.$this->generateURL(58 + $id+1);
      }
      $this->db->where('id', $id);
      $this->db->update('links', [
        'shorturl' => $shorturl
      ]);

      return $shorturl;
    }

    return false;
  }

  private function generateURL($int)
  {
    $alphabet = "0123456789bcdefgknopqrstuxzBCDEFGKNOPQRSTUXZ";

    $base58_string = "";
		$base = strlen($alphabet);

		while($int >= $base) {
			$div = floor($int / $base);
			$mod = $int % $base;
			$base58_string = $alphabet{$mod} . $base58_string;
			$int = $div;
		}

		if($int)
    {
      $base58_string = $alphabet{intval($int)} . $base58_string;
    }

		return $base58_string;
  }

  public function checkData($data)
  {
      return true;
  }
}
