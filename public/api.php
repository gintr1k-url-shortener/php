<?php

/* Устанавливаем формат ответа JSON */
header('Content-Type: application/json');

/* Подключаем engine.php */
require_once __DIR__.'/../classes/engine.php';

/* Создаем класс движка */
$Engine = new LinkShortenerEngine;

/* Проверяем принятые данные */
switch ($_GET['method']) {
  /* Метод addShortUrlAnon */
  case 'addShortUrlAnon':
    if (isset($_POST['URL']))
    {
      if ($Engine->checkData($_POST['URL']) === true)
      {
        $url = $_POST['URL'];
        $result = $Engine->addShortUrl($url, -1, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']);

        stop(true, [
          'shorturl' => rtrim($domain, '/').$result
        ]);
      }

    }
    /* Параметры не переданы */
    stop(false, 'Required parameters do not sended');
    break;

}

/* Метода не существует */
stop(false, 'Method do not exists');

function stop($success, $data)
{
  echo json_encode([
    'success' => $success,
    ($success === false ? 'err_text' : 'data') => $data
  ]);

  exit;
}
