<?php
/* Подключаем engine.php */
require_once __DIR__.'/../classes/engine.php';

/* Создаем класс движка */
$Engine = new LinkShortenerEngine;

$url = $Engine->findRedirect();
if ($url !== false)
{
  # Защищаемся от XSS-атак
  $url = addslashes($url);
  $url = htmlspecialchars($url);
  
  header ('Location: '.$url);
}
else
{
  header("HTTP/1.0 404 Not Found");
  include('errors/404.html');
}
